package zoo.animals;

/**
 * Created by Max on 030, 30.07.2019.
 */
public abstract class Fish extends Animal {

    public Fish(){
        super();
    }

    @Override
    public void eat() {
    }

    public void eat(String food) {
    }

    public void sleep(){
        System.out.println(name + " never sleep");
    }

    public abstract void swim();
}
