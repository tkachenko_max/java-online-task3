package zoo.animals;

public class Parrot extends Bird {

    public Parrot() {
        this("bright parrot");
    }

    public Parrot(String name){
        super(name);
        this.necessaryFood = "plants";
    }

    @Override
    public void eat(String food) {
        if(food ==necessaryFood ){
            System.out.println(name + " pecks " + food + "!");
        }else{
            System.out.println(name + " will not eat " + food + "." + "Try to feed him with plants." );
        }
    }
    public void sleep(){
        System.out.println(name + " sleep ...");
    }

    @Override
    public void fly() {
        System.out.println(name + " flies from branch to branch.");
    }
}
