package zoo.animals;

/**
 * Created by Max on 030, 30.07.2019.
 */
public abstract class Bird extends Animal{

    public Bird(String name){
        super(name);
    }
    public void eat(String food) {
    }
     public abstract void fly();

}
