package zoo.animals;

/**
 * Created by Max on 030, 30.07.2019.
 */
public class Animal {
    String name;
    String necessaryFood = "anything";

    public Animal() {

    }

    public Animal(String name){
        this.name = name;
    }

    public void eat(){ }
    public void sleep(){
        System.out.println(name + " sleep...hrrr");
    }

}
