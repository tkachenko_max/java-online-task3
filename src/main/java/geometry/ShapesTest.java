package geometry;

import geometry.shapes.*;
/**
 * Created by Max on 029, 29.07.2019.
 */
public class ShapesTest {
    public static void main(String[] args) {
        Circle circle = new Circle();
        System.out.println(circle);

        circle = new Circle("red", true, 3.5);
        System.out.println(circle);

        circle = new Circle("green", true, 4);
        System.out.println(circle);

        circle = new Circle("blue", true, 5.5);
        System.out.println(circle);

        Rectangle rectangle = new Rectangle();
        System.out.println(rectangle);

        rectangle = new Rectangle(4.5, 4.5, "red", true);
        System.out.println(rectangle);

        rectangle = new Rectangle(4.5, 4.5, "blue", true);
        System.out.println(rectangle);

        rectangle = new Rectangle(5.0, 5.0, "yellow", true);
        System.out.println(rectangle);

        Triangle triangle = new Triangle();
        System.out.println(triangle);

        triangle = new Triangle("orange", true, 4.0, 4.0, 4.0 );
        System.out.println(triangle);

        triangle = new Triangle("purple", true, 4.0, 4.0, 4.0 );
        System.out.println(triangle);

        triangle = new Triangle("grey", true, 4.0, 4.0, 4.0 );
        System.out.println(triangle);



    }
}
