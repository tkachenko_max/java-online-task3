package zoo.animals;

public class Eagle extends Bird {

    public Eagle (){
        this("Proud Eagle");
    }
    public Eagle(String name){
        super(name);
        this.necessaryFood = "meat";
    }
    @Override
    public void eat(String food) {
        if(food ==necessaryFood ){
            System.out.println(name + " pecks " + food + "!");
        }else{
            System.out.println(name + " will not eat " + food + "." + "Try to feed him with meat.");
        }
    }
    @Override
    public void fly() {
        System.out.println(name + " flies very high!");
    }
}
