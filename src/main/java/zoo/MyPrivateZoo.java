package zoo;

import zoo.animals.*;

public class MyPrivateZoo {
    public static void main(String[] args) {
        Parrot parrot = new Parrot("Boa");
        parrot.eat("meat");
        parrot.fly();
        parrot.sleep();

        Eagle eagle = new Eagle();
        eagle.fly();
        eagle.eat("plants");
        eagle.sleep();

    }
}
